package com.example.Example.controller

import com.example.Example.repository.ApplicationUserRepository
import com.example.Example.entity.common.ApplicationUser
import com.example.Example.entity.enums.Role
import com.example.Example.service.UserService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/users")
class UserController (
        private val userService: UserService
) {
    @PostMapping("/sign-up")
    fun signUp(@RequestBody user: ApplicationUser) {
        userService.register(user)
    }
}