package com.example.Example.controller.admin

import com.example.Example.entity.common.Room
import com.example.Example.service.RoomService
import org.springframework.web.bind.annotation.*
import java.time.DayOfWeek

@RestController()
@RequestMapping("/api/v1/admin/rooms")
class RoomControllerAdmin(
        private val roomService: RoomService
) {

    @GetMapping
    fun getAll(): List<Room> {
        return roomService.getAll();
    }

    @GetMapping("{id}")
    fun getInfo(@PathVariable id: Long): Room {
        return roomService.getInfo(id);
    }

    @PostMapping
    fun create(@RequestBody room: Room): Room {
        return roomService.create(room);
    }

    @PutMapping("{id}")
    fun update(
            @PathVariable id: Long,
            @RequestBody room: Room
    ): Room {
        return roomService.update(id, room);
    }

    @DeleteMapping("{id}")
    fun delete(@PathVariable id: Long) {
        roomService.delete(id);
    }
}