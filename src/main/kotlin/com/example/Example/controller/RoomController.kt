package com.example.Example.controller

import com.example.Example.entity.common.Room
import com.example.Example.entity.common.Schedule
import com.example.Example.entity.dto.ScheduleDto
import com.example.Example.service.RoomService
import com.example.Example.service.RoomScheduleService
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/rooms")
class RoomController (
        private val roomService: RoomService,
        private val roomScheduleService: RoomScheduleService
) {
    @GetMapping
    fun getAll(): List<Room> {
        return roomService.getAll();
    }

    @GetMapping("{id}")
    fun getInfo(@PathVariable id: Long): Room {
        return roomService.getInfo(id);
    }

    @PostMapping("book")
    fun book(authentication: Authentication, @RequestBody schedule: ScheduleDto) {
        roomScheduleService.toBookRoom(schedule, authentication.name);
    }

    @PutMapping("edit")
    fun edit(@RequestBody schedule: ScheduleDto) {
        roomScheduleService.editSchedule(schedule);
    }

    @PostMapping("free/{id}")
    fun free(@PathVariable id: Long) {
        roomScheduleService.toFreeRoom(id);
    }

    @GetMapping("reserved")
    fun getReserved(): List<Schedule> {
        return roomScheduleService.getReservedRooms();
    }
}