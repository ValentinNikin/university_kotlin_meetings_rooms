package com.example.Example.repository

import com.example.Example.entity.common.Room
import com.example.Example.entity.common.Schedule
import org.springframework.data.jpa.repository.JpaRepository

interface RoomScheduleRepository : JpaRepository<Schedule, Long> {
    fun findAllByRoom(room: Room): List<Schedule>?;
}