package com.example.Example.repository

import com.example.Example.entity.common.Room
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface RoomRepository : JpaRepository<Room, Long> {
}