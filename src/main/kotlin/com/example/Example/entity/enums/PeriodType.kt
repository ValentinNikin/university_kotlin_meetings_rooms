package com.example.Example.entity.enums

enum class PeriodType {
    ONE_DAY, PERIOD;
}