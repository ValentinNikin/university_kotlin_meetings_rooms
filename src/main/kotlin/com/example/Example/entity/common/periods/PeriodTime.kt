package com.example.Example.entity.common.periods

import java.time.LocalDate
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
data class PeriodTime (
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long?,

        @NotNull
        val dateFrom: LocalDate,

        @NotNull
        val dateTo: LocalDate
) {
        @NotNull
        @OneToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.ALL), orphanRemoval=true)
        var weekdays: List<WeekdayTime> = listOf()
}