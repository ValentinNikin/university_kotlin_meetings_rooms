package com.example.Example.entity.common

import com.example.Example.entity.common.ApplicationUser
import com.example.Example.entity.common.Room
import com.example.Example.entity.common.periods.OneTimePeriod
import com.example.Example.entity.common.periods.PeriodTime
import com.example.Example.entity.enums.PeriodType
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table
data class Schedule (
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long?,

        @OneToOne(orphanRemoval=true)
        @NotNull
        var room: Room,

        @OneToOne(orphanRemoval=true)
        @NotNull
        val occupiedByUser: ApplicationUser,

        @NotNull
        @Enumerated(EnumType.STRING)
        var periodType: PeriodType,

        @OneToOne(cascade = arrayOf(CascadeType.ALL), orphanRemoval=true)
        var oneTimePeriod: OneTimePeriod?,

        @OneToOne(cascade = arrayOf(CascadeType.ALL), orphanRemoval=true)
        var periodTime: PeriodTime?
)