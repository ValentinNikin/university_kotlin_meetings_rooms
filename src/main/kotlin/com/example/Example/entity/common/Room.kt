package com.example.Example.entity.common

import javax.persistence.*
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
@Table
data class Room (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long?,

    @Size(max = 100)
    @NotNull
    var title: String,

    @NotNull
    @Min(1)
    var countPlaces: Int
)