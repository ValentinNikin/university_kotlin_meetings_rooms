package com.example.Example.entity.common

import com.example.Example.entity.enums.Role
import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
class ApplicationUser (
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long?,

        @Size(max = 200)
        @NotNull
        val username: String,

        @Size(max = 200)
        @NotNull
        var password: String,

        @ElementCollection(fetch = FetchType.EAGER)
        @CollectionTable(name = "user_role", joinColumns = [JoinColumn(name = "user_id")])
        @Enumerated(EnumType.STRING)
        var roles: MutableSet<Role>? = mutableSetOf(Role.USER),

        @Size(max = 200)
        @NotNull
        var fullName: String,

        @Size(max = 100)
        @NotNull
        var position: String,

        @Size(max = 200)
        @NotNull
        var department: String
)