package com.example.Example.entity.common.periods

import java.time.DayOfWeek
import java.time.LocalTime
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
data class WeekdayTime (
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long?,

        @NotNull
        @Enumerated(EnumType.STRING)
        val weekday: DayOfWeek,

        @NotNull
        val timeFrom: LocalTime,

        @NotNull
        val timeTo: LocalTime

//        @ManyToOne(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.ALL))
//        @JoinColumn
//        val periodTime: PeriodTime
)