package com.example.Example.entity.common.periods

import java.time.LocalDate
import java.time.LocalTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.NotNull

@Entity
data class OneTimePeriod (
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long?,

        @NotNull
        val date: LocalDate,

        @NotNull
        val timeFrom: LocalTime,

        @NotNull
        val timeTo: LocalTime
)