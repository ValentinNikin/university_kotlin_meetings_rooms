package com.example.Example.entity.dto.periods

import java.time.DayOfWeek
import java.time.LocalTime

data class WeekdayTimeDto (
        val weekday: DayOfWeek,
        val timeFrom: LocalTime,
        val timeTo: LocalTime
)