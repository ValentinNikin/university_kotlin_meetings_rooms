package com.example.Example.entity.dto.periods

import java.time.LocalDate

data class PeriodTimeDto (
        val dateFrom: LocalDate,
        val dateTo: LocalDate,
        val weekdays: Collection<WeekdayTimeDto>
)