package com.example.Example.entity.dto

import com.example.Example.entity.dto.periods.OneTimePeriodDto
import com.example.Example.entity.dto.periods.PeriodTimeDto
import com.example.Example.entity.enums.PeriodType

data class ScheduleDto(
        val id: Long,
        val roomId: Long,
        val periodType: PeriodType,
        val oneTimePeriod: OneTimePeriodDto?,
        val periodTime: PeriodTimeDto?
)