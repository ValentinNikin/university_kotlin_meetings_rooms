package com.example.Example.entity.dto.periods

import java.time.LocalDate
import java.time.LocalTime

data class OneTimePeriodDto (
        val date: LocalDate,
        val timeFrom: LocalTime,
        val timeTo: LocalTime
)