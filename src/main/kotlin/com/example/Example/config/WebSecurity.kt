package com.example.Example.config

import com.example.Example.config.SecurityConstants.SIGN_UP_URL
import com.example.Example.filters.JWTAuthenticationFilter
import com.example.Example.filters.JWTAuthorizationFilter
import com.example.Example.service.impl.UserDetailsServiceImpl
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource


@EnableWebSecurity
class WebSecurity (
        private val userDetailsService: UserDetailsServiceImpl,
        private val bCryptPasswordEncoder: BCryptPasswordEncoder
) : WebSecurityConfigurerAdapter() {

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.cors()
                .and()
                    .csrf().disable()
                    .authorizeRequests()
                    .antMatchers(
                            "/swagger-ui.html",
                            "/webjars/**",
                            "/v2/api-docs",
                            "/swagger-resources",
                            "/swagger-resources/configuration/ui",
                            "/swagger-resources/configuration/security")
                    .permitAll()
                    .antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
                    .antMatchers("/api/v1/admin/**").hasAuthority("ADMIN")
                    .anyRequest().authenticated()
                .and()
                    .addFilter(JWTAuthenticationFilter(authenticationManager()))
                    .addFilter(JWTAuthorizationFilter(authenticationManager())) // this disables session creation on Spring Security
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder)

    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource? {
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", CorsConfiguration().applyPermitDefaultValues())
        return source
    }
}