package com.example.Example.filters

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.example.Example.config.SecurityConstants.HEADER_STRING
import com.example.Example.config.SecurityConstants.SECRET
import com.example.Example.config.SecurityConstants.TOKEN_PREFIX
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.util.*
import java.util.stream.Collectors
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JWTAuthorizationFilter (
        private val authManager: AuthenticationManager
) : BasicAuthenticationFilter(authManager) {

    override fun doFilterInternal(req: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        val header = req.getHeader(HEADER_STRING)
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, response)
            return
        }
        val authentication = getAuthentication(req)
        SecurityContextHolder.getContext().authentication = authentication
        chain.doFilter(req, response)
    }

    private fun getAuthentication(request: HttpServletRequest): UsernamePasswordAuthenticationToken? {
        val token = request.getHeader(HEADER_STRING)
        if (token != null) {
            // parse the token.
            val tokenParsed = JWT.require(Algorithm.HMAC512(SECRET.toByteArray(Charsets.UTF_8)))
                    .build()
                    .verify(token.replace(TOKEN_PREFIX, ""))
            val user = tokenParsed.subject
            val roles = tokenParsed.getClaim("roles").asString()
            val authorities: Collection<GrantedAuthority?> = Arrays.asList(roles.split(",")).stream()
                    .map { authority -> SimpleGrantedAuthority(authority[0])}.collect(Collectors.toList())
            return if (user != null) {
                UsernamePasswordAuthenticationToken(user, null, authorities)
            } else null
        }
        return null
    }
}