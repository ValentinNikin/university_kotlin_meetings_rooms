package com.example.Example.filters

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm.HMAC512
import com.example.Example.config.SecurityConstants.EXPIRATION_TIME
import com.example.Example.config.SecurityConstants.HEADER_STRING
import com.example.Example.config.SecurityConstants.SECRET
import com.example.Example.config.SecurityConstants.TOKEN_PREFIX
import com.example.Example.entity.common.ApplicationUser
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.io.IOException
import java.util.*
import java.util.stream.Collectors
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JWTAuthenticationFilter (
    private val authManager: AuthenticationManager
): UsernamePasswordAuthenticationFilter() {
    override fun attemptAuthentication(
            req: HttpServletRequest,
            res: HttpServletResponse
    ): Authentication {
        try {
            val creds = ObjectMapper()
                    .readValue(req.inputStream, ApplicationUser::class.java)
            return this.authManager.authenticate(
                    UsernamePasswordAuthenticationToken(
                            creds.username,
                            creds.password,
                            creds.roles)
            )
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
    }

    override fun successfulAuthentication(
            req: HttpServletRequest,
            res: HttpServletResponse,
            chain: FilterChain,
            auth: Authentication
    ) {
        val authorities: String  = (auth.principal as User).authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(","))
        val token: String = JWT.create()
                .withSubject((auth.principal as User).username)
                .withClaim("roles", authorities)
                .withExpiresAt(Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(HMAC512(SECRET.toByteArray(Charsets.UTF_8)))
        res.addHeader(HEADER_STRING, TOKEN_PREFIX.toString() + token)
    }
}
