package com.example.Example.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED)
class ScheduleAlreadyBusyException(
        private val what: String
): RuntimeException(what) {
}