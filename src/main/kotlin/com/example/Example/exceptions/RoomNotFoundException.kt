package com.example.Example.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED)
class RoomNotFoundException(
        private val id: Long
): RuntimeException("Room with id $id not found") {
}