package com.example.Example.service

import com.example.Example.entity.common.Schedule
import com.example.Example.entity.dto.ScheduleDto

interface RoomScheduleService {
    fun toBookRoom(schedule: ScheduleDto, username: String)
    fun toFreeRoom(scheduleId: Long)
    fun editSchedule(schedule: ScheduleDto)
    fun getReservedRooms(): List<Schedule>
}