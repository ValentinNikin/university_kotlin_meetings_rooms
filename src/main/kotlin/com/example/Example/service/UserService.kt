package com.example.Example.service

import com.example.Example.entity.common.ApplicationUser

interface UserService {
    fun register(user: ApplicationUser);
}