package com.example.Example.service.impl

import com.example.Example.entity.common.Room
import com.example.Example.exceptions.NotFoundException
import com.example.Example.repository.RoomRepository
import com.example.Example.service.RoomService
import org.springframework.beans.BeanUtils
import org.springframework.stereotype.Service

@Service
class RoomServiceImpl(
        private val roomRepository: RoomRepository
) : RoomService {
    override fun getAll(): List<Room> {
        return roomRepository.findAll();
    }

    override fun getInfo(id: Long): Room {
        return roomRepository.findById(id).orElseThrow{ NotFoundException() };
    }

    override fun create(room: Room): Room {
        return roomRepository.save(room);
    }

    override fun update(id: Long, room: Room): Room {
        val roomDb = roomRepository.findById(id).orElseThrow { NotFoundException() };
        BeanUtils.copyProperties(room, roomDb, "id");

        return roomRepository.save(roomDb);
    }

    override fun delete(id: Long) {
        val roomDb = roomRepository.findById(id).orElseThrow { NotFoundException() };
        roomRepository.delete(roomDb);
    }
}