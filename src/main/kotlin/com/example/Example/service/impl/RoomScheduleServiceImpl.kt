package com.example.Example.service.impl

import com.example.Example.entity.common.Schedule
import com.example.Example.entity.common.periods.OneTimePeriod
import com.example.Example.entity.common.periods.PeriodTime
import com.example.Example.entity.common.periods.WeekdayTime
import com.example.Example.entity.dto.ScheduleDto
import com.example.Example.entity.enums.PeriodType
import com.example.Example.exceptions.RoomNotFoundException
import com.example.Example.exceptions.ScheduleAlreadyBusyException
import com.example.Example.exceptions.ScheduleNotFoundException
import com.example.Example.repository.ApplicationUserRepository
import com.example.Example.repository.RoomRepository
import com.example.Example.repository.RoomScheduleRepository
import com.example.Example.service.RoomScheduleService
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.lang.RuntimeException
import java.time.LocalDate
import java.time.LocalTime

@Service
class RoomScheduleServiceImpl(
        private val roomScheduleRepository: RoomScheduleRepository,
        private val roomRepository: RoomRepository,
        private val applicationUserRepository: ApplicationUserRepository
): RoomScheduleService {

    @Transactional
    override fun toBookRoom(schedule: ScheduleDto, username: String) {
        val roomDB = roomRepository.findById(schedule.roomId);
        val userDB = applicationUserRepository.findByUsername(username);

        var newScheduleDB: Schedule? = null;

        if (schedule.periodType == PeriodType.ONE_DAY) {
            val oneType = OneTimePeriod(null, schedule.oneTimePeriod!!.date, schedule.oneTimePeriod!!.timeFrom, schedule.oneTimePeriod!!.timeTo)
            newScheduleDB = Schedule(null, roomDB.get(), userDB!!, schedule.periodType, oneType, null)
        } else if (schedule.periodType == PeriodType.PERIOD) {
            val periodType = PeriodTime(
                    null,
                    schedule.periodTime!!.dateFrom,
                    schedule.periodTime!!.dateTo)

            val weekdays = schedule.periodTime!!.weekdays.map { it -> WeekdayTime(null, it.weekday, it.timeFrom, it.timeTo ) }

            periodType.weekdays = weekdays

            newScheduleDB = Schedule(null, roomDB.get(), userDB!!, schedule.periodType, null, periodType)
        }

        if (newScheduleDB == null) {
            throw RuntimeException("Room shedule was not created")
        }

        val scheduleForRoom = roomScheduleRepository.findAllByRoom(roomDB.get());

        for (shedule in scheduleForRoom!!.iterator()) {
            if (checkBusy(shedule, newScheduleDB)) {
                throw ScheduleAlreadyBusyException("This time already busy by ${shedule.occupiedByUser.fullName}");
            }
        }

        roomScheduleRepository.save(newScheduleDB)
    }

    @Transactional
    override fun editSchedule(editSchedule: ScheduleDto) {
        val findSchedule = roomScheduleRepository.findByIdOrNull(editSchedule.id) ?: throw ScheduleNotFoundException(editSchedule.id);
        val roomDB = roomRepository.findByIdOrNull(editSchedule.roomId) ?: throw RoomNotFoundException(editSchedule.roomId);

        findSchedule.room = roomDB;
        findSchedule.periodType = editSchedule.periodType;

        if (editSchedule.periodType == PeriodType.ONE_DAY) {
            findSchedule.oneTimePeriod = OneTimePeriod(null, editSchedule.oneTimePeriod!!.date, editSchedule.oneTimePeriod!!.timeFrom, editSchedule.oneTimePeriod!!.timeTo)
            findSchedule.periodTime = null;
        } else if (editSchedule.periodType == PeriodType.PERIOD) {
            val periodType = PeriodTime(
                    null,
                    editSchedule.periodTime!!.dateFrom,
                    editSchedule.periodTime!!.dateTo)

            val weekdays = editSchedule.periodTime!!.weekdays.map { it -> WeekdayTime(null, it.weekday, it.timeFrom, it.timeTo ) }

            periodType.weekdays = weekdays

            findSchedule.periodTime = periodType;
            findSchedule.oneTimePeriod = null;
        }

        val scheduleForRoom = roomScheduleRepository.findAllByRoom(roomDB);

        for (schedule in scheduleForRoom!!.iterator()) {
            if (schedule.id != findSchedule.id) {
                if (checkBusy(schedule, findSchedule)) {
                    throw ScheduleAlreadyBusyException("This time already busy by ${schedule.occupiedByUser.fullName}");
                }
            }
        }

        roomScheduleRepository.save(findSchedule);
    }

    @Transactional
    override fun toFreeRoom(scheduleId: Long) {
        val findSchedule = roomScheduleRepository.findByIdOrNull(scheduleId);

        if (findSchedule != null) {
            roomScheduleRepository.delete(findSchedule);
        } else {
            throw ScheduleNotFoundException(scheduleId)
        }
    }

    @Transactional
    override fun getReservedRooms(): List<Schedule> {
        val allSchedule = roomScheduleRepository.findAll();
        allSchedule.map { it -> it.occupiedByUser.password = ""; it.occupiedByUser.roles = mutableSetOf() };
        return allSchedule;
    }

    private fun checkBusy(existSchedule: Schedule, newSchedule: Schedule): Boolean {
        if (existSchedule.periodType == PeriodType.ONE_DAY && newSchedule.periodType == PeriodType.ONE_DAY) {
            if (existSchedule.oneTimePeriod!!.date == newSchedule.oneTimePeriod!!.date) {
                return compareTimePeriod(
                        existSchedule.oneTimePeriod!!.timeFrom,
                        existSchedule.oneTimePeriod!!.timeTo,
                        newSchedule.oneTimePeriod!!.timeFrom,
                        newSchedule.oneTimePeriod!!.timeTo);
            }
            return false;
        }

        if (existSchedule.periodType == PeriodType.ONE_DAY && newSchedule.periodType == PeriodType.PERIOD) {
            if (existSchedule.oneTimePeriod!!.date >= newSchedule.periodTime!!.dateFrom &&
                    existSchedule.oneTimePeriod!!.date <= newSchedule.periodTime!!.dateTo) {
                val existScheduleDayOfWeek = existSchedule.oneTimePeriod!!.date.dayOfWeek;
                val weekdayExist = newSchedule.periodTime!!.weekdays.find { it -> it.weekday == existScheduleDayOfWeek }
                if (weekdayExist != null) {
                    return compareTimePeriod(
                            existSchedule.oneTimePeriod!!.timeFrom,
                            existSchedule.oneTimePeriod!!.timeTo,
                            weekdayExist!!.timeFrom,
                            weekdayExist!!.timeTo);
                }
            }
            return false;
        }

        if (existSchedule.periodType == PeriodType.PERIOD && newSchedule.periodType == PeriodType.ONE_DAY) {
            if (newSchedule.oneTimePeriod!!.date >= existSchedule.periodTime!!.dateFrom &&
                    newSchedule.oneTimePeriod!!.date <= existSchedule.periodTime!!.dateTo) {
                val newScheduleDayOfWeek = newSchedule.oneTimePeriod!!.date.dayOfWeek;
                val weekdayExist = existSchedule.periodTime!!.weekdays.find {it -> it.weekday == newScheduleDayOfWeek }
                if (weekdayExist != null) {
                    return compareTimePeriod(
                            newSchedule.oneTimePeriod!!.timeFrom,
                            newSchedule.oneTimePeriod!!.timeTo,
                            weekdayExist!!.timeFrom,
                            weekdayExist!!.timeTo);
                }
            }
            return false;
        }

        if (existSchedule.periodType == PeriodType.PERIOD && newSchedule.periodType == PeriodType.PERIOD) {
            if (existSchedule.periodTime!!.dateTo < newSchedule.periodTime!!.dateFrom ||
                    existSchedule.periodTime!!.dateFrom > newSchedule.periodTime!!.dateTo) {
                return false;
            }

            var start: LocalDate? = null;
            var stop: LocalDate? = null;

            if (existSchedule.periodTime!!.dateFrom <= newSchedule.periodTime!!.dateFrom) {
                start = newSchedule.periodTime!!.dateFrom;
            } else {
                start = existSchedule.periodTime!!.dateTo;
            }

            if (existSchedule.periodTime!!.dateTo <= newSchedule.periodTime!!.dateTo) {
                stop = existSchedule.periodTime!!.dateTo;
            } else {
                stop = newSchedule.periodTime!!.dateTo;
            }

            do {
                val weekdayExist = existSchedule.periodTime!!.weekdays.find { it -> it.weekday == start!!.dayOfWeek};
                val weekdayNew = newSchedule.periodTime!!.weekdays.find { it -> it.weekday == start!!.dayOfWeek};

                if (weekdayExist != null && weekdayNew != null) {
                    if (compareTimePeriod(
                            weekdayExist!!.timeFrom,
                            weekdayExist!!.timeTo,
                            weekdayNew!!.timeFrom,
                            weekdayNew!!.timeTo)) {
                        return true;
                    }
                }
                start = start!!.plusDays(1);
            } while (start != stop);

            return false;
        }

        return false;
    }

    private fun compareTimePeriod(
            firstFrom: LocalTime, firstTo: LocalTime,
            secondFrom: LocalTime, secondTo: LocalTime): Boolean {
        if (firstTo <= secondFrom) {
            return false;
        }

        if (firstFrom >= secondTo) {
            return false;
        }

        return true;
    }
}