package com.example.Example.service.impl

import com.example.Example.entity.common.ApplicationUser
import com.example.Example.entity.enums.Role
import com.example.Example.exceptions.UserAlreadyExistException
import com.example.Example.repository.ApplicationUserRepository
import com.example.Example.service.UserService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserServiceImpl (
        private val applicationUserRepository: ApplicationUserRepository,
        private val bCryptPasswordEncoder: BCryptPasswordEncoder
): UserService {
    override fun register(user: ApplicationUser) {
        val userExist: ApplicationUser? = applicationUserRepository.findByUsername(user.username!!)

        if (userExist != null) {
            throw UserAlreadyExistException("User with username \"${user.username}\" already exist")
        }

        user.password = bCryptPasswordEncoder.encode(user.password);
        user.roles!!.add(Role.USER);
        applicationUserRepository.save(user);
    }
}