package com.example.Example.service

import com.example.Example.entity.common.Room


interface RoomService {
    fun getAll(): List<Room>
    fun getInfo(id: Long): Room
    fun create(room: Room): Room
    fun update(id: Long, room: Room): Room
    fun delete(id: Long)
}